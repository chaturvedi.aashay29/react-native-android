import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  Button,
  Alert,
  View,
  FlatList,
  StyleSheet,
  Text,
  Image,
  StatusBar,
  TextInput,
  PermissionsAndroid,
} from 'react-native';

var RNFS = require('react-native-fs');

import RNFetchBlob from 'rn-fetch-blob';

const JsonDataScreen = () => {
  const [dataFromFile, setDataFromFile] = useState('');
  const [value, onChangeText] = React.useState('name');

  const createFileInAndroid = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('Permission granted');

        const fs = RNFetchBlob.fs;
        const dirs = RNFetchBlob.fs.dirs;

        const NEW_FILE_PATH = dirs.DownloadDir + '/test.txt';

        fs.unlink(NEW_FILE_PATH)
          .then(() => {
            console.log('successfully deleted file');
          })
          .catch((err) => {
            console.log('error in deleting file = ', err);
          });

        let content =
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eget velit placerat, ' +
          'interdum neque nec, laoreet risus. Proin ac iaculis tortor. Duis sed purus aliquam, dapibus orci eu,' +
          'mollis ipsum. Sed iaculis est sit amet eros blandit, a sagittis quam dignissim. Pellentesque viverra ' +
          'quam et turpis sodales laoreet. Phasellus ullamcorper sapien sit amet cursus sodales. Fusce fringilla ' +
          'magna nec turpis lobortis accumsan. Sed consequat molestie orci, eu scelerisque nunc facilisis vel. ' +
          'Donec sit amet turpis malesuada est euismod tempor. Sed varius leo tortor, ut interdum enim convallis eu. ' +
          'Sed fringilla vulputate porttitor.';

        fs.createFile(NEW_FILE_PATH, content, 'utf8');
      } else {
        console.log('Permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  };

  const readFileFromAndroid = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('Permission granted');

        const fs = RNFetchBlob.fs;
        // const base64 = RNFetchBlob.base64;
        const dirs = RNFetchBlob.fs.dirs;

        const PATH_TO_READ = dirs.DownloadDir + '/test.txt';
        fs.readFile(PATH_TO_READ, 'utf8').then((data) => {
          setDataFromFile(data);
        });
      } else {
        console.log('Permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  };

  const saveNewDataToFileInAndroid = async (nameOfWriter) => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('Permission granted');

        const fs = RNFetchBlob.fs;
        const dirs = RNFetchBlob.fs.dirs;

        const NEW_FILE_PATH = dirs.DownloadDir + '/test.txt';

        fs.unlink(NEW_FILE_PATH)
          .then(() => {
            console.log('successfully deleted file');
          })
          .catch((err) => {
            console.log('error in deleting file = ', err);
          });

        let content =
          `Data by ${nameOfWriter} Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eget velit placerat, ` +
          'interdum neque nec, laoreet risus. Proin ac iaculis tortor. Duis sed purus aliquam, dapibus orci eu,' +
          'mollis ipsum. Sed iaculis est sit amet eros blandit, a sagittis quam dignissim. Pellentesque viverra ' +
          'quam et turpis sodales laoreet. Phasellus ullamcorper sapien sit amet cursus sodales. Fusce fringilla ' +
          'magna nec turpis lobortis accumsan. Sed consequat molestie orci, eu scelerisque nunc facilisis vel. ' +
          'Donec sit amet turpis malesuada est euismod tempor. Sed varius leo tortor, ut interdum enim convallis eu. ' +
          'Sed fringilla vulputate porttitor.';

        fs.createFile(NEW_FILE_PATH, content, 'utf8');
      } else {
        console.log('Permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  };

  useEffect(() => {
    createFileInAndroid();
  }, []);

  return (
    <View style={styles.container}>
      <View style={styles.innerContainer}>
        <Text style={styles.title}>Data From File:</Text>
        <Text>{dataFromFile}</Text>
        <View style={styles.button}>
          <Button
            title="Read File from Download Folder in Android emulator"
            onPress={() => readFileFromAndroid()}
          />
        </View>
        <View style={styles.button}>
          <TextInput
            style={{height: 40, borderColor: 'gray', borderWidth: 1, margin: 5}}
            onChangeText={(text) => onChangeText(text)}
            value={value}
          />
          <Button
            title="Save New Data In File In Download Folder"
            onPress={() => saveNewDataToFileInAndroid(value)}
          />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#CCCCCC',
  },
  innerContainer: {
    marginVertical: 30,
  },
  title: {
    textAlign: 'center',
    fontSize: 30,
    fontWeight: 'bold',
  },
  button: {
    margin: 10,
  },
});

export default JsonDataScreen;
