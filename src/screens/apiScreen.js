import React, {useEffect, useState} from 'react';
import axios from 'axios';
import {
  SafeAreaView,
  View,
  FlatList,
  StyleSheet,
  Text,
  Image,
  StatusBar,
  ActivityIndicator,
} from 'react-native';

const Item = ({title, sourceUrl}) => (
  <View style={styles.item}>
    <Text style={styles.title}>{title}</Text>
    <Image style={styles.tinyLogo} source={{uri: sourceUrl}} />
  </View>
);

const ApiScreen = () => {
  const [data, setData] = useState('');
  const [isLoading, setIsLoading] = useState(true);

  const callApi = () => {
    axios.get('https://picsum.photos/v2/list').then((response) => {
      setData(response.data);
      setIsLoading(false);
    });
  };
  const getDataFromApi = () => {
    setIsLoading(true);
    setTimeout(() => callApi(), 5000);
  };

  useEffect(() => {
    getDataFromApi();
  }, []);

  const renderItem = ({item}) => (
    <Item title={item.author} sourceUrl={item.download_url} />
  );

  return (
    <SafeAreaView style={styles.container}>
      {isLoading ? (
        <ActivityIndicator color={'black'} />
      ) : (
        <FlatList
          data={data}
          renderItem={renderItem}
          keyExtractor={(item) => item.id}
        />
      )}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
  },
  item: {
    // backgroundColor: '#f9c2ff',
    padding: 5,
    marginVertical: 5,
    marginHorizontal: 10,
    borderColor: 'black',
    borderWidth: 2,
  },
  title: {
    fontSize: 16,
  },
  tinyLogo: {
    width: 50,
    height: 50,
  },
});

export default ApiScreen;
