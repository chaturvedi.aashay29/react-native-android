import React from 'react';

import GallerySwiper from 'react-native-gallery-swiper';

const HomeScreen = () => {
  return (
    <GallerySwiper
      images={[
        {
          source: require('fPeerTask/src/images/ww.jpg'),
          dimensions: {width: 1080, height: 1920},
        },
        {
          uri:
            'https://luehangs.site/pic-chat-app-images/beautiful-blond-blonde-hair-478544.jpg',
          dimensions: {width: 1080, height: 1920},
        },
        {
          uri:
            'https://luehangs.site/pic-chat-app-images/animals-avian-beach-760984.jpg',
          dimensions: {width: 1080, height: 1920},
        },
        {
          source: require('fPeerTask/src/images/stones.jpg'),
          dimensions: {width: 1080, height: 1920},
        },
        {
          source: require('fPeerTask/src/images/earth.png'),
          dimensions: {width: 1080, height: 1920},
        },
      ]}
      initialNumToRender={2}
      sensitiveScroll={false}
    />
  );
};

export default HomeScreen;
