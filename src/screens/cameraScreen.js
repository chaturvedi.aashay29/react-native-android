import React, {useState} from 'react';
import {SafeAreaView, View, Image} from 'react-native';

import Camera from '../component/Camera';

const CameraScreen = () => {
  return (
    <SafeAreaView style={{flex: 1}}>
      <Camera />
    </SafeAreaView>
  );
};

export default CameraScreen;
