import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import HomeScreen from './src/screens/homeScreen';
import LocationScreen from './src/screens/locationScreen';
import ApiScreen from './src/screens/apiScreen';
import JsonDataScreen from './src/screens/jsonDataScreen';
import CameraScreen from './src/screens/cameraScreen';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {NavigationContainer} from '@react-navigation/native';

const Tab = createBottomTabNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Tab.Navigator>
        <Tab.Screen name="Home" component={HomeScreen} />
        <Tab.Screen name="Location" component={LocationScreen} />
        <Tab.Screen name="API" component={ApiScreen} />
        <Tab.Screen name="Data JSON" component={JsonDataScreen} />
        <Tab.Screen name="Camera" component={CameraScreen} />
      </Tab.Navigator>
    </NavigationContainer>
  );
};

export default App;
